//2015110705
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NAME_SIZE 30
#define MAX_QUEUE_SIZE 5

typedef struct {
	int id;
	char name[MAX_NAME_SIZE];
} element;

element queue[MAX_QUEUE_SIZE];
int rear = -1;
int front = -1;

void addq(element item);
element deleteq();

void queueFull();
element queueEmpty();

int main()
{
	element tempElement;
	char buffer[100];
	char* result;
	char delimiter[] = " ";

	printf("<< linear queue operations where MAX_QUEUE_SIZE is 5>> \n");
	printf("add 1 Jung \n");
	printf("delete \n");
	printf("\n**************************************************\n");

	while (1) {
		//line input
		gets(buffer);

		//operator check
		result = strtok(buffer, delimiter);

		if (strcmp(result, "add") == 0) {
			//id input
			result = strtok(NULL, delimiter);
			if (result == NULL) {
				fprintf(stderr, "Input Error \n");
				exit(EXIT_FAILURE);
			}

			tempElement.id = atoi(result);

			//name input
			result = strtok(NULL, delimiter);
			if (result == NULL) {
				fprintf(stderr, "Input Error \n");
				exit(EXIT_FAILURE);
			}
			strcpy(tempElement.name, result);

			//만약에 찌꺼기가 남았을 경우 에러처리
			result = strtok(NULL, delimiter);
			if (result != NULL) {
				fprintf(stderr, "Input Error \n");
				exit(EXIT_FAILURE);
			}

			addq(tempElement);
		}
		else if (strcmp(result, "delete") == 0) {
			deleteq();
		}
		else {
			printf("wrong command! try again! \n");
		}
	}


	return 0;
}

void addq(element item)
{
	if (rear == MAX_QUEUE_SIZE - 1)
		queueFull();
	queue[++rear] = item;
}

element deleteq()
{
	if (front == rear)
		return queueEmpty();

	return queue[++front];
}

void queueFull()
{
	int i;
	element deleteElement;

	if (front == -1 && rear == MAX_QUEUE_SIZE - 1) {
		fprintf(stderr, "Queue is full, cannot add element! \n");
		for (i = 0; i < MAX_QUEUE_SIZE; i++) {
			deleteElement = deleteq();
			printf("%d %s \n", deleteElement.id, deleteElement.name);
		}
		exit(EXIT_FAILURE);
	}
	else {
		int count = 0;
		printf("array shifting... \n");
		for (i = front + 1; i < MAX_QUEUE_SIZE; i++) {
			queue[count] = queue[i];
			count++;
		}
		rear -= (front + 1);
		front = -1;
	}
}

element queueEmpty()
{
	element errKey;
	errKey.id = 0;
	strcpy(errKey.name, "Error Key");

	fprintf(stderr, "Queue is empty, cannot delete element \n");
	exit(EXIT_FAILURE);

	return errKey;
}