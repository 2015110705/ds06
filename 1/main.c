//2015110705
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STACK_SIZE 5
#define MAX_NAME_SIZE 30

typedef struct {
	int id;
	char name[MAX_NAME_SIZE];
} element;

element stack[MAX_STACK_SIZE];
int top = -1;

element pop();
void push(element);
void stackFull();
element stackEmpty();

int main()
{
	char buffer[100];
	char* result;
	char delimiter[] = " ";
	element tempElement = { 0, "" };

	printf("<< stack operations where MAX_STACK_SIZE is 5 >> \n");
	printf("push 1 Jung \npop \n");
	printf("\n****************************************\n");

	while (1) {
		//line input
		gets(buffer);

		//operator check
		result = strtok(buffer, delimiter);

		if (strcmp(result, "push") == 0) {
			//id input
			result = strtok(NULL, delimiter);
			if (result == NULL) {
				fprintf(stderr, "Input Error \n");
				exit(EXIT_FAILURE);
			}

			tempElement.id = atoi(result);

			//name input
			result = strtok(NULL, delimiter);
			if (result == NULL) {
				fprintf(stderr, "Input Error \n");
				exit(EXIT_FAILURE);
			}
			strcpy(tempElement.name, result);

			//만약에 찌꺼기가 남았을 경우 에러처리
			result = strtok(NULL, delimiter);
			if (result != NULL) {
				fprintf(stderr, "Input Error \n");
				exit(EXIT_FAILURE);
			}

			push(tempElement);
		}
		else if (strcmp(result, "pop") == 0) {
			pop();
		}
		else {
			printf("wrong command! try again! \n");
		}
	}

	return 0;
}

void push(element item)
{
	if (top >= MAX_STACK_SIZE - 1)
		stackFull();
	stack[++top] = item;
}

element pop()
{
	if (top == -1) {
		return stackEmpty();
	}

	return stack[top--];
}

void stackFull()
{
	int i;
	element popElement;

	fprintf(stderr, "Stack is full, cannot add element \n");
	printf("current stack elements : \n");
	for (i = MAX_STACK_SIZE - 1; i >= 0; i--) {
		popElement = pop();
		printf("%d %s \n", popElement.id, popElement.name);
	}
	exit(EXIT_FAILURE);
}

element stackEmpty()
{
	element errKey;
	errKey.id = 0;
	strcpy(errKey.name, "Error Key");

	fprintf(stderr, "Stack is empty, cannot delete element \n");
	exit(EXIT_FAILURE);

	return errKey;
}
