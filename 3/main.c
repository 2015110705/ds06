//2015110705
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NAME_SIZE 30
#define MAX_QUEUE_SIZE 2

typedef struct {
	int id;
	char name[MAX_NAME_SIZE];
} element;

element *queue;
int capacity = 2;
int rear = 0;
int front = 0;

element deleteq();
void addq(element item);
void queueFull();
element queueEmpty();
void copy(element*, element*, element*);

int main()
{
	element tempElement;
	char buffer[100];
	char* result;
	char delimiter[] = " ";

	queue = (element*)malloc(sizeof(element) * capacity);

	printf("<< linear queue operations where the initial capacity is 2>> \n");
	printf("add 1 Jung \n");
	printf("delete \n");
	printf("\n**************************************************\n");

	while (1) {
		//line input
		gets(buffer);

		//operator check
		result = strtok(buffer, delimiter);

		if (strcmp(result, "add") == 0) {
			//id input
			result = strtok(NULL, delimiter);
			if (result == NULL) {
				fprintf(stderr, "Input Error \n");
				exit(EXIT_FAILURE);
			}

			tempElement.id = atoi(result);

			//name input
			result = strtok(NULL, delimiter);
			if (result == NULL) {
				fprintf(stderr, "Input Error \n");
				exit(EXIT_FAILURE);
			}
			strcpy(tempElement.name, result);

			//만약에 찌꺼기가 남았을 경우 에러처리
			result = strtok(NULL, delimiter);
			if (result != NULL) {
				fprintf(stderr, "Input Error \n");
				exit(EXIT_FAILURE);
			}

			addq(tempElement);
		}
		else if (strcmp(result, "delete") == 0) {
			element deleteQueue = deleteq();
			printf("deleted item : %d %s \n", deleteQueue.id, deleteQueue.name);
		}
		else {
			printf("wrong command! try again! \n");
		}
	}

	return 0;
}

element deleteq()
{
	element item;
	if (front == rear) {
		return queueEmpty();
	}

	front = (front + 1) % capacity;
	return queue[front];
}

void addq(element item)
{
	rear = (rear + 1) % capacity;
	if (front == rear) {
		queueFull();
	}
	queue[rear] = item;
}

void queueFull()
{
	int start;

	element* newQueue = (element*)malloc(2 * capacity* sizeof(element));

	start = (front + 1) % capacity;
	rear--;

	if (start < 2) {
		copy(queue + start, queue + capacity, newQueue);
		copy(queue, queue + rear + 1, newQueue + capacity - start);
	}
	else {
		copy(queue + start, queue + capacity, newQueue);
		copy(queue, queue + rear + 1, newQueue + capacity - start);
	}

	front = 2 * capacity - 1;
	rear = capacity - 1;
	capacity *= 2;
	free(queue);
	queue = newQueue;
	printf("queue capacity is doubled \n");
	printf("current queue capacity is %d \n", capacity);
}

element queueEmpty()
{
	element errKey;
	errKey.id = 0;
	strcpy(errKey.name, "Error Key");

	fprintf(stderr, "Queue is empty, cannot delete element \n");
	exit(EXIT_FAILURE);

	return errKey;
}

void copy(element* a, element* b, element* c)
{
	int i = 0;

	for (i = 0; a != b; i++) {
		c[i] = *a;
		a++;
	}
}